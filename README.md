# Presentaciones hechas por mi
Con <3 para la comunidad

## Proyectos

* Hashcrack Telegram Bot - Bsides Chile
    - Promo: https://www.youtube.com/watch?v=f4HZyFTkldc
    - Proyecto: https://gitlab.com/vay3t/hashcrack-telegram-bot
    - Video: https://youtu.be/YwhioxWlUWo?t=24918
    - Presentación: https://gitlab.com/vay3t/Presentaciones/-/blob/main/BsidesCL/hashcracktelegrambot.pdf

## Serie de Bash para pentesting

* Bash Scripting by Vay3t - L4tin-HTB
    * Video: https://www.youtube.com/watch?v=IT4GsN2IyGY
* Recon/pentesting con Bash - Hackmeeting BO
    * Video: https://youtu.be/lqSWOakmnqA?t=12682
    * Presentación: https://gitlab.com/vay3t/Presentaciones/-/blob/main/HackmeetingBO/BashHackmeetingBO.pdf

### Entradas de blog

* Creando un notificador en Telegram con Bash
   * https://vay3t.medium.com/creando-un-notificador-en-telegram-con-bash-b842490610


## Hacking con Python

* SQL Injection con Python - GopiLabz
    * Video: https://www.youtube.com/watch?v=eXU_DQWsY3w
    * Presentación: https://gitlab.com/vay3t/Presentaciones/-/blob/main/GopiLabz/sqli_con_python.pdf
    * Exploit: https://gitlab.com/vay3t/Presentaciones/-/blob/main/GopiLabz/final_exploit.py

